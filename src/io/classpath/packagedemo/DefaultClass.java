package io.classpath.packagedemo;

public class DefaultClass {
    public static void main(String[] args) {
        PublicClass obj = new PublicClass();
        // cannot access private variables outside the class
        // System.out.printf("Private variable %d", obj.privateVariable);
        System.out.printf("Default variable %d", obj.defautlValue);
        System.out.printf("Protected variable %d", obj.protectedVariable);
        System.out.printf("Public variable %d", obj.publicVariable);    }
}