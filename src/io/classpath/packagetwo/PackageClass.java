package io.classpath.packagetwo;

import io.classpath.packagedemo.PublicClass;
import static io.classpath.packagedemo.PublicClass.PI;

public class PackageClass extends io.classpath.packagedemo.PublicClass {
    public static void main(String[] args) {
        //fully qualified name
        io.classpath.packagedemo.PublicClass obj = new io.classpath.packagedemo.PublicClass();

        // cannot access private variables outside the class
        // System.out.printf("Private variable %d", obj.privateVariable);
       // System.out.printf("Default variable %d", obj.defautlValue);

        //you still cannot access protected value using the class reference
        //System.out.printf("Protected variable %d", obj.protectedVariable);

        //can access the protected varibles/method only with the subclass object
        PackageClass subObj = new PackageClass();
        System.out.printf("Protected variable %d ", subObj.protectedVariable);
        System.out.printf("Public variable %d", obj.publicVariable);
        System.out.println(" Public variable "+ PI);
    }
}