package io.classpath.interfacedemo;

public interface PaymentGateway {
    //all methods are public and abstract
    boolean transferAmount(String from, String to, String notes);

}