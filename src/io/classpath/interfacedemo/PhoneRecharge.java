package io.classpath.interfacedemo;

public interface PhoneRecharge {
    boolean recharge(String phoneNumber, double amount);
}