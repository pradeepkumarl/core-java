package io.classpath.interfacedemo;

public class Paytm implements PaymentGateway, PhoneRecharge {
    @Override
    public boolean transferAmount(String from, String to, String notes) {
        System.out.println("Transfering money from "+ from +" to "+ to+", notes: "+ notes);
        System.out.println("You have been rewarded with 50 Rs cashback");
        return true;
    }

    @Override
    public boolean recharge(String phoneNumber, double amount) {
        System.out.println("Patym recharge");
        return false;
    }
}